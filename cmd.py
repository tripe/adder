from subprocess import Popen as popen
from subprocess import PIPE as PIPE
from sys import stdout as out
from sys import stderr as err
from sys import exit

# TODO: sort out the flush

def cmd_run(c, check=True, loud=True):
  if loud: print(c)
  out.flush()
  p = popen(c, shell=True, stdout=PIPE,stderr=PIPE)
  o, e = map(strip, p.communicate())
  r = p.wait()
  if check: cmd_check(r, o, e)
  return (r, o, e)

def cmd_check(r, o, e):
  if (r != 0):
    print "\n".join(filter(None, ["FAILED", o, e]))
    exit(r)
  out.flush()

def strip(s):
  return s.rstrip()

