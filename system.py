from os.path    import dirname
from os.path    import abspath
from os.path    import join
from os.path    import isfile
from os.path    import isdir
from os         import makedirs
from cmd        import cmd_run         as cmd
from sys        import exit
from files      import check_file
from files      import check_absolute
from files      import set_file_attrs
from files      import to_relative
from fileinc    import file_next
from substitute import substitute_file
from substitute import substitute

def link_create((s, d)):
  check_file("e", "y", s)
  check_file("e", "n", d)
  check_absolute(s)
  check_absolute(d)
  cmd("ln -s %s %s" % (s, d))

def dir_create((d, o, m)):
  check_file("e", "n", d)
  check_absolute(d)
  cmd("mkdir %s" % d)
  set_file_attrs(d, o, m)

def file_install((f, o, m), (loc, dic)):
  rel = to_relative(f)
  i = get_install_file(loc, rel)
  check_file("f", "n", f)
  check_file("f", "y", i)
  check_file("d", "y", dirname(f))
  substitute_file(i, f, dic)
  set_file_attrs(f, o, m)

def file_replace((f, o, m), (loc, dic, save)):
  check_absolute(f)
  check_absolute(save)
  dst = join(save, to_relative(f))
  check_file("d", "y", save)
  check_file("f", "y", f)
  check_file("f", "n", dst)
  save_copy(f, dst)
  cmd("rm %s" % f)
  file_install((f, o, m), (loc, dic))

def file_substitute((f, o, m, e), (dic, save, inc)):
  check_absolute(f)
  check_absolute(save)
  check_absolute(inc)
  check_file("d", "y", save)
  check_file("d", "y", inc)
  check_file("f", "y", f)
  rel = to_relative(f)
  dsave = join(save, rel)
  dinc = join(inc, rel)
  ddinc = dirname(dinc)
  if not isfile(dsave): save_copy(f, dsave)
  if not isdir(ddinc): makedirs(ddinc)
  nxt = save_copy(f, file_next(dinc))
  exp = substitute(e, dic)
  cmd("sed -i -e '%s' %s" % (exp, f))
  set_file_attrs(f, o, m)

def ch_mod_own((f, o, m)):
  check_file("e", "y", f)
  set_file_attrs(f, o, m)

def save_copy(src, dst):
  cmd("mkdir -p %s" % dirname(dst))
  cmd("cp %s %s" % (src, dst))

def get_install_file(loc, rel):
  r = "%s/%s/%s" % (abspath(dirname(__file__)), loc, rel)
  check_file("f", "y", r)
  return r
