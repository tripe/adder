from cmd    import cmd_run  as cmd
from string import Template as template

def user_create(d):
  create(d, "useradd ${flags} -u ${uid} -c \'${comment}\' -d ${home} -g ${gid} -s ${shell} ${name}")
  

def group_create(d):
  create(d, "groupadd -g ${gid} ${name}") 

def create(d, s):
  cmd(template(s).substitute(d))
