from jinja2 import Template as template
from jinja2 import StrictUndefined as strict
from cmd    import cmd_run  as cmd

def substitute_file(s, d, dic):
  o = cmd("cat %s" % s, loud=False)[1]
  open(d, "w").write(substitute(o, dic) + '\n')

def substitute(s, dic):
  t = template(s, undefined=strict)
  return t.render(dic)
