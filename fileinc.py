from cmd      import cmd_run  as cmd
from os.path  import dirname
from files    import check_file

def file_next(f):
  check_file("d", "y", dirname(f))
  ids = cmd("ls %s.[0-9][0-9][0-9][0-9]" % f, False, False)[1].split()
  return "%s.%d" % (f, 1000+len(ids))
