from cmd import cmd_run as cmd
from sys import exit

OPTIONS= "def"
EXISTS= "yn"

def check_file(o, e, n):
  if not (isa(o, OPTIONS)): barf (o, e, n)
  if not (isa(e,  EXISTS)): barf (o, e, n)

  if e == "y": look("[   -%s %s ] || exit 1" % (o, n), "[%s] %s does not exist" % (o, n))
  if e == "n": look("[ ! -%s %s ] || exit 1" % (o, n), "[%s] %s already exists" % (o, n))

def check_absolute(f):
  if not f[0] == '/': exit("file %s not absolute" % f)

def set_file_attrs(f, o, m):
  cmd("chown %s %s" % (o, f), loud=False)
  cmd("chmod %s %s" % (m, f), loud=False)

def to_relative(f):
  check_absolute(f)
  return f[1:]

def look(c, m):
  if cmd(c, False, False)[0] != 0: exit(m)

def barf(o, e, n):
  exit("usage: check_file [%s] [%s] <file> ... %s %s %s" % (OPTIONS, EXISTS, o, e, n))

def isa(c, cs):
  return cs.find(c) != -1
