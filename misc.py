from cmd import cmd_run as cmd
from os  import environ as env

CMDS = {
  "pkg-install"        : "apt-get install -y %s",
  "apache-mod-install" : "a2enmod %s",
  "service-restart"    : "service %s restart",
  "runlevel-number"    : "runlevel | sed 's/^.*\(.\)$/\\1/'",
  "service-list"       : "ls /etc/rc%s.d/S??%s",
}

def install_setup():
  env['DEBIAN_FRONTEND'] = 'noninteractive'

# TODO: CENTOS dupe
def centos___install_setup():
  pass

def deb_conf(c):
  cmd("echo %s | debconf-set-selections" % c)

def apt_get_refresh():
  cmd("apt-get upgrade -y")
  cmd("apt-get update")

# TODO: CENTOS dupe
def centos___yum_refresh():
  cmd("yum update -y")

def apt_get_install(p):
  cmd(CMDS["pkg-install"] % p)

# TODO: CENTOS dupe
def centos___yum_install(p):
  cmd("yum install -y %s" % p)

def apache_mod_install(m):
  cmd(CMDS["apache-mod-install"] % m)
  service_restart("apache2")

# Use or lose
def service_restart(s):
  cmd(CMDS["service-restart"] % s)

# Use or lose
def service_install(s):
#  See README in {/etc/rc2.d,/etc/init.d}
#  ...
#  l = cmd(CMDS["runlevel-number"])
#  r = cmd(CMDS["service-list"] % (l, s), False)
#  if (r): return
  return False
