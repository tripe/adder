from user     import group_create
from user     import user_create
from system   import link_create
from system   import ch_mod_own
from system   import dir_create
from system   import file_install
from system   import file_replace
from system   import file_substitute
from keygen   import ssh_keygen
from misc     import deb_conf
from misc     import apt_get_install
from misc     import apt_get_refresh
from misc     import apache_mod_install
from misc     import install_setup

from misc     import centos___install_setup
from misc     import centos___yum_refresh
from misc     import centos___yum_install
from database import centos___database_setup

def maps(f, s):
  map(f, s)

def mapt(f, s, t):
  for i in s: f(i, t)

# TODO: quite likely this is too obtuse...
def deb_conf_all(l)           : maps(deb_conf, l)
def apt_get_install_all(l)    : maps(apt_get_install, l)
def apache_mod_install_all(l) : maps(apache_mod_install, l)
def users_create_all(l)       : maps(user_create, l)
def groups_create_all(l)      : maps(group_create, l)
def link_create_all(l)        : maps(link_create, l)
def ch_mod_own_all(l)         : maps(ch_mod_own, l)
def dir_create_all(l)         : maps(dir_create, l)
def file_install_all(l, t)    : mapt(file_install, l, t)
def file_replace_all(l, t)    : mapt(file_replace, l, t)
def file_substitute_all(l, t) : mapt(file_substitute, l, t)

def centos___yum_install_all(l) : maps(centos___yum_install, l)
