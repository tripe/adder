from os.path import isfile
from cmd     import cmd_run as cmd

def ssh_keygen(u):
  key_file = "/home/%s/.ssh/id_rsa" % u
  # FAIL IF KEY EXISTS...
  if not isfile(key_file):
    cmd("su - %s -c 'ssh-keygen -q -P \"\" -t rsa -b 4096 -f %s'" % (u, key_file))
